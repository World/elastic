# Friulian translation for elastic.
# Copyright (C) 2024 elastic's COPYRIGHT HOLDER
# This file is distributed under the same license as the elastic package.
# Fabio Tomat <f.t.public@gmail.com>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: elastic main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/elastic/-/issues\n"
"POT-Creation-Date: 2024-04-24 11:21+0000\n"
"PO-Revision-Date: 2024-04-24 18:55+0200\n"
"Last-Translator: \n"
"Language-Team: Friulian <fur@li.org>\n"
"Language: fur\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.4.2\n"

#: data/app.drey.Elastic.desktop.in:3 data/app.drey.Elastic.metainfo.xml.in:11
#: src/window.ui:6
msgid "Elastic"
msgstr "Elastic"

#: data/app.drey.Elastic.desktop.in:4 data/app.drey.Elastic.metainfo.xml.in:12
msgid "Design spring animations"
msgstr "Dissegne animazions a suste"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/app.drey.Elastic.desktop.in:14
msgid "spring;animation;animations;GNOME;GTK;Adwaita;"
msgstr "suste;animazion;animazions;GNOME;GTK;Adwaita;"

#: data/app.drey.Elastic.gschema.xml:14 src/preferences-dialog.ui:15
msgid "Use Damping"
msgstr "Dopre smuartiment"

#: data/app.drey.Elastic.gschema.xml:15
msgid "Whether to use damping instead of damping ratio."
msgstr "Indiche se doprâ il smuartiment invezit dal tas di smuartiment."

#: data/app.drey.Elastic.gschema.xml:19
msgid "Last Language"
msgstr "Ultime lenghe"

#: data/app.drey.Elastic.gschema.xml:20
msgid "Last selected language."
msgstr "Ultime lenghe selezionade."

#: data/app.drey.Elastic.metainfo.xml.in:15
msgid ""
"Elastic allows to design and export spring physics-based animations to use "
"with libadwaita."
msgstr ""
"Elastic al permet di dissegnâ e espuartâ animazions basadis su la fisiche "
"des sustis, par doprâlis cun libadwaita."

#: data/app.drey.Elastic.metainfo.xml.in:19
msgid "Features:"
msgstr "Funzionalitâts:"

#: data/app.drey.Elastic.metainfo.xml.in:21
msgid "Preview translation, rotation and scaling transformations."
msgstr "Anteprime di traslazion, rotazion e trasformazions di scjale."

#: data/app.drey.Elastic.metainfo.xml.in:22
msgid "See the animation curve and duration on a graph."
msgstr "Viôt la curve di animazion e durade suntun grafich."

#: data/app.drey.Elastic.metainfo.xml.in:23
msgid "Drag a handle to see it return back with the spring physics."
msgstr ""
"Strissine une mantie par viodile torna inadûr cu la fisiche des sustis."

#: data/app.drey.Elastic.metainfo.xml.in:24
msgid "Export C, JavaScript, Python, Vala or Rust code."
msgstr "Espuarte codiç C, JavaScript, Python, Vala o Rust."

#: data/app.drey.Elastic.metainfo.xml.in:31
msgid "Initial screen"
msgstr "Schermade iniziâl"

#: data/app.drey.Elastic.metainfo.xml.in:35
msgid "Animation graph"
msgstr "Grafic de animazion"

#: data/app.drey.Elastic.metainfo.xml.in:39
msgid "Interactive preview"
msgstr "Anteprime interative"

#: data/app.drey.Elastic.metainfo.xml.in:43
msgid "Code export"
msgstr "Esportazion codiç"

#: src/gtk/help-overlay.ui:12
msgctxt "shortcut window"
msgid "General"
msgstr "Gjenerâl"

#: src/gtk/help-overlay.ui:15
msgctxt "shortcut window"
msgid "Open Menu"
msgstr "Vierç menù"

#: src/gtk/help-overlay.ui:21
msgctxt "shortcut window"
msgid "Show Preferences"
msgstr "Mostre preferencis"

#: src/gtk/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Mostre scurtis"

#: src/gtk/help-overlay.ui:33
msgctxt "shortcut window"
msgid "Close Current Window"
msgstr "Siere chest barcon"

#: src/gtk/help-overlay.ui:39
msgctxt "shortcut window"
msgid "Quit App"
msgstr "Jes de aplicazion"

#: src/gtk/help-overlay.ui:47
msgctxt "shortcut window"
msgid "Editor"
msgstr "Editôr"

#: src/gtk/help-overlay.ui:50
msgctxt "shortcut window"
msgid "Basic View"
msgstr "Viodude di base"

#: src/gtk/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Interactive View"
msgstr "Viodude interative"

#: src/gtk/help-overlay.ui:62
msgctxt "shortcut window"
msgid "Run Animation"
msgstr "Eseguìs animazion"

#: src/gtk/help-overlay.ui:68
msgctxt "shortcut window"
msgid "Toggle Graph"
msgstr "Comute grafic"

#: src/gtk/help-overlay.ui:74
msgctxt "shortcut window"
msgid "Copy Snippet"
msgstr "Copie snippet"

#: src/gtk/help-overlay.ui:80
msgctxt "shortcut window"
msgid "Reset to Defaults"
msgstr "Ristabilìs ai predefinîts"

#: src/application.vala:45
msgid "translator-credits"
msgstr "Fabio Tomat <f.t.public@gmail.com>, 2024"

#: src/basic-view.ui:32
msgid "Initial Velocity"
msgstr "Velocitât iniziâl"

#: src/basic-view.ui:33
msgid "Information About Initial Velocity"
msgstr "Informazions su la velocitât iniziâl"

#: src/basic-view.ui:34
msgid ""
"Initial velocity affects the beginning of the animation curve without "
"changing duration. This is useful when finishing gestures with an animation, "
"or to make it bounce more in either direction."
msgstr ""
"La velocitât iniziâl e influìs sul inizi de curve di animazion cence cambiâ "
"la durade. Chest al è util cuant che si finissin i mots cuntune animazion, o "
"par fâle saltâ di plui in dutis dôs lis direzions."

#: src/basic-view.ui:60
msgid "Toggle Graph"
msgstr "Comute grafic"

#: src/basic-view.ui:72
msgid "Run Animation"
msgstr "Eseguìs animazion"

#: src/export-view.ui:200
msgid "_Copy Snippet"
msgstr "_Copie snippet"

#: src/graph-view.vala:97
#, c-format
msgid "Min: %.2lf"
msgstr "Min: %.2lf"

#: src/graph-view.vala:102
#, c-format
msgid "Max: %.2lf"
msgstr "Max: %.2lf"

#: src/graph-view.vala:125
#, c-format
msgid "Duration: %.0lf ms"
msgstr "Durade: %.0lf ms"

#: src/preferences-dialog.ui:10
msgid ""
"Damping determines how much the spring oscillates. Damping ratio is a more "
"convenient parameter adjusted to behave the same way regardless of mass and "
"stiffness, and most of the time it’s easier to work with.\n"
"\n"
"If you need to import values from another framework that uses damping, you "
"can switch to damping instead."
msgstr ""
"Il smuartiment al determine trop che la suste e niçule. Il tas di "
"smuartiment al è un parametri che al ven regolât in mût plui convenient par "
"compuartâsi te stesse maniere cence che si tegni di cont la masse e la "
"rigjiditât, te plui part dai câs al è plui facil di doprâ.\n"
"\n"
"Se ti covente impuartâ valôrs di un altri ambient di lavôr che al dopre i "
"smuartiments, al è pussibil passâ invezit ai smuartiments."

#: src/preferences-dialog.ui:16
msgid "Prefer damping instead of damping ratio"
msgstr "Preferìs il smuartiment invezit che il tas di smuartiment"

#: src/properties-view.ui:21
msgid "Damping"
msgstr "Smuartiment"

#: src/properties-view.ui:22
msgid "Information About Damping"
msgstr "Informazions sul smuartiment"

#: src/properties-view.ui:23
msgid ""
"How much the spring oscillates. Set it to the marked value to bring the "
"spring to rest as soon as possible with no oscillation, lower values to "
"create oscillation, higher values to slow the spring down."
msgstr ""
"Trop che la suste e niçule. Metile al valôr segnât par puartâ la suste ae "
"cundizion di polse a pene pussibil cence ossilazion, valôrs plui bas par "
"creâ ossilazion, plui alts par cuietâ la suste."

#: src/properties-view.ui:39
msgid "Damping Ratio"
msgstr "Tas di smuartiment"

#: src/properties-view.ui:40
msgid "Information About Damping Ratio"
msgstr "Informazions sul tas di smuartiment"

#: src/properties-view.ui:41
msgid ""
"How much the spring oscillates. Set it to 1 to bring the spring to rest as "
"soon as possible with no oscillation, lower values to create oscillation, "
"higher values to slow the spring down."
msgstr ""
"Trop che la suste e niçule. Metile a 1 par puartâ la suste ae cundizion di "
"polse a pene pussibil cence ossilazion, valôr plui bas par creâ ossilazion, "
"plui alts par cuietâ la suste."

#: src/properties-view.ui:58
msgid "Mass"
msgstr "Masse"

#: src/properties-view.ui:59
msgid "Information About Mass"
msgstr "Informazions su la masse"

#: src/properties-view.ui:60
msgid ""
"The mass of the attached object. The higher the mass is, the longer the "
"spring takes to stop."
msgstr ""
"La masse dal ogjet tacât. Plui al è alt il valôr de masse, plui timp la "
"suste e dopre par fermâsi."

#: src/properties-view.ui:75
msgid "Stiffness"
msgstr "Rigjiditât"

#: src/properties-view.ui:76
msgid "Information About Stiffness"
msgstr "Informazions su la rigjiditât"

#: src/properties-view.ui:77
msgid ""
"The stiffness of the spring. The higher the stiffness is, the faster the "
"spring contracts."
msgstr ""
"La rigjiditât de suste. Plui al è alt il valôr de rigjiditât, plui svelte la "
"suste si contrai."

#: src/properties-view.ui:92
msgid "Epsilon"
msgstr "Epsilon"

#: src/properties-view.ui:93
msgid "Information About Epsilon"
msgstr "Informazions sul Epsilon"

#: src/properties-view.ui:94
msgid ""
"Determines when to stop the simulation. This value should be large enough "
"that the animation doesn’t last too long, but small enough that there’s no "
"visible jump at the end."
msgstr ""
"Al determine cuant fermâ la simulazion. Chest valôr al à di sei avonde grant "
"par fa in mût che la animazion no duri masse, ma ancje avonde piçul di no fâ "
"viodi salts ae fin."

#: src/window.ui:21
msgid "_Reset"
msgstr "_Ristabilìs"

#: src/window.ui:22
msgid "Reset to Defaults"
msgstr "Ristabilìs ai predefinîts"

#: src/window.ui:33
msgid "Main Menu"
msgstr "Menù principâl"

#: src/window.ui:78
msgid "Basic"
msgstr "Base"

#: src/window.ui:88
msgid "Interactive"
msgstr "Interatîf"

#: src/window.ui:131
msgid "_Preferences"
msgstr "_Preferencis"

#: src/window.ui:135
msgid "_Keyboard Shortcuts"
msgstr "_Scurtis di tastiere"

#: src/window.ui:139
msgid "_About Elastic"
msgstr "_Informazions su Elastic"

#: src/window.vala:71
msgid "Copied to clipboard"
msgstr "Copiât intes notis"

#~ msgid "Alice Mikhaylenko"
#~ msgstr "Alice Mikhaylenko"
