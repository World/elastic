// This file is part of Elastic. License: GPL-3.0+.

public class Elastic.Application : Adw.Application {
    public Application () {
        Object (
            application_id: "app.drey.Elastic",
            flags: ApplicationFlags.FLAGS_NONE
        );
    }

    construct {
        ActionEntry[] action_entries = {
            { "about", on_about_action },
            { "preferences", on_preferences_action },
            { "quit", quit }
        };
        add_action_entries (action_entries, this);

        set_accels_for_action ("app.quit", {"<primary>q"});
        set_accels_for_action ("app.preferences", {"<primary>comma"});
        set_accels_for_action ("editor.basic-view", {"<alt>1"});
        set_accels_for_action ("editor.interactive-view", {"<alt>2"});
        set_accels_for_action ("editor.run-animation", {"<primary>r"});
        set_accels_for_action ("editor.toggle-graph", {"<primary>g"});
        set_accels_for_action ("editor.copy-snippet", {"<shift><primary>c"});
        set_accels_for_action ("editor.reset", {"<primary>t"});
    }

    public override void activate () {
        base.activate ();

        var win = active_window ?? new Window (this);

        win.present ();
    }

    private void on_about_action () {
        string[] developers = { "Alice Mikhaylenko" };
        string[] designers = { "Tobias Bernard" };
        var appdata = "/app/drey/Elastic/app.drey.Elastic.metainfo.xml";

        var about = new Adw.AboutDialog.from_appdata (appdata, null) {
            developers = developers,
            designers = designers,
            translator_credits = _("translator-credits"),
            copyright = "© 2021, 2023 Alice Mikhaylenko",
        };

        about.present (active_window);
    }

    private void on_preferences_action () {
        var win = new PreferencesDialog ();

        win.present (active_window);
    }
}
